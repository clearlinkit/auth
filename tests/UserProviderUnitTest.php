<?php

use PHPUnit\Framework\TestCase;
use Clearlink\Auth\Providers\UserProvider;

class UserProviderUnitTest extends TestCase
{
    public function setup()
    {
        parent::setup();

        $this->service = Mockery::mock('Clearlink\Auth\AuthService');
        $this->cache = Mockery::mock('Illuminate\Cache\Repository');

        $this->provider = new UserProvider($this->service, $this->cache);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function test_retrieve_by_credentials()
    {
        $credentials = [
            'jwt' => $jwtJson = str_random(40)
        ];

        $this->service->shouldReceive('getAuthenticatedUser')
            ->once()
            ->with($jwtJson)
            ->andReturn($user = Mockery::mock('Clearlink\Users\User'));

        $this->service->shouldReceive('getCustomUserRepository')
            ->once();

        $this->assertInstanceOf('Clearlink\Auth\User', $this->provider->retrieveByCredentials($credentials));
    }

    public function test_retrieve_from_cache()
    {
        $credentials = [
            'jwt' => $jwtJson = str_random(40),
            'cacheKey' => 'key'
        ];

        $this->cache->shouldReceive('get')
            ->once()
            ->with('key')
            ->andReturn($user = Mockery::mock('Clearlink\Users\User'));
        $user->shouldReceive('getId')
            ->andReturn(5);

        $this->service->shouldReceive('getCustomUserRepository')
            ->once();

        $this->assertInstanceOf('Clearlink\Auth\User', $this->provider->retrieveByCredentials($credentials));
    }

    public function test_caches_dto_user()
    {
        $credentials = [
            'jwt' => $jwtJson = str_random(40),
            'cacheKey' => 'key'
        ];

        $this->cache->shouldReceive('get')
            ->once()
            ->with('key');

        $this->service->shouldReceive('getAuthenticatedUser')
            ->once()
            ->with($jwtJson)
            ->andReturn($user = Mockery::mock('Clearlink\Users\User'));
        $user->shouldReceive('getId')
            ->andReturn(5);

        $this->cache->shouldReceive('put')
            ->once()
            ->with('key', $user, 20);

        $this->service->shouldReceive('getCustomUserRepository')
            ->once();

        $this->assertInstanceOf('Clearlink\Auth\User', $this->provider->retrieveByCredentials($credentials));
    }

    public function test_get_custom_user()
    {
        $provider = new ProviderStub($this->service, $this->cache);

        $this->service->shouldReceive('getCustomUserRepository')
            ->once()
            ->andReturn($repo = Mockery::mock());

        $repo->shouldReceive('find')
            ->once()
            ->with(5)
            ->andReturn($customUser = new UserStub);

        $user = Mockery::mock('Clearlink\Users\User');
        $user->shouldReceive('getId')
            ->andReturn(5);

        $this->assertEquals($customUser, $provider->stubGetCustomUser($user));
    }

    /**
     * @expectedException \Exception
     */
    public function test_exception_on_no_custom_model_found()
    {
        $provider = new ProviderStub($this->service, $this->cache);

        $this->service->shouldReceive('getCustomUserRepository')
            ->once()
            ->andReturn($repo = Mockery::mock());

        $repo->shouldReceive('find')
            ->once()
            ->with(5);

        $user = Mockery::mock('Clearlink\Users\User');
        $user->shouldReceive('getId')
            ->andReturn(5);

        $provider->stubGetCustomUser($user);
    }
}

class UserStub implements \Illuminate\Contracts\Auth\Authenticatable
{
    use Illuminate\Auth\Authenticatable, \Clearlink\Auth\Traits\ClearlinkUser;

    public function getId()
    {
        return 5;
    }
}

class ProviderStub extends UserProvider
{
    public function stubGetCustomUser($user)
    {
        return $this->getCustomUser($user);
    }
}

