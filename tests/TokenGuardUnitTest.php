<?php

use PHPUnit\Framework\TestCase;
use Clearlink\Auth\Guards\TokenGuard;

class TokenGuardUnitTest extends TestCase
{
    public function setup()
    {
        $this->request = Mockery::mock('Illuminate\Http\Request');
        $this->provider = Mockery::mock('Illuminate\Contracts\Auth\UserProvider');
        $this->validator = Mockery::mock('Clearlink\Auth\JWT\TokenValidator');

        $this->guard = new TokenGuard($this->provider, $this->validator);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function test_valid_user()
    {
        
        $this->request->shouldReceive('bearerToken')
            ->once()
            ->andReturn($token = 'TestToken');

        $this->provider->shouldReceive('retrieveByCredentials')
            ->once()
            ->with(['jwt' => $token, 'cacheKey' => $token])
            ->andReturn($user = Mockery::mock());

        $this->validator->shouldReceive('validate')
            ->once()
            ->with($token)
            ->andReturn(\Clearlink\Auth\JWT\TokenValidator::VALID);

        $this->validator->shouldReceive('isOldStyleToken')
            ->andReturn(true);

        $this->assertEquals($user, $this->guard->user($this->request));
    }

    public function test_not_bearer()
    {
        $this->request->shouldReceive('bearerToken')
            ->once()
            ->andReturn(null);

        $this->assertNull($this->guard->user($this->request));
    }

    public function test_not_valid()
    {
        $this->request->shouldReceive('bearerToken')
            ->once()
            ->andReturn($token = 'Test Token');

        $this->validator->shouldReceive('validate')
            ->once()
            ->with($token)
            ->andReturn(\Clearlink\Auth\JWT\TokenValidator::INVALID);

        $this->assertNull($this->guard->user($this->request));
    }
}
