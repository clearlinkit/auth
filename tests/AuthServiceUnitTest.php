<?php

use Clearlink\Auth\AuthService;
use PHPUnit\Framework\TestCase;

function env($key)
{
    return AuthServiceUnitTest::$retval;
}

class AuthServiceUnitTest extends TestCase
{
    public static $retval;

    public function setUp()
    {
        parent::setUp();

        self::$retval = null;

        $this->config = Mockery::mock('Illuminate\Config\Repository');
        $this->cache = Mockery::mock('Illuminate\Cache\Repository');
        $this->factory = Mockery::mock('Clearlink\DTO\Factory');
        $this->client = Mockery::mock('GuzzleHttp\Client');

        $this->service = new AuthService($this->config, $this->cache, $this->factory, $this->client);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function test_set_custom_token_authentication_function()
    {
        $func = function () {};

        $this->assertTrue($this->service->setCustomTokenAuthenticationFunction($func));

        return $this->service;
    }

    /**
     * @depends test_set_custom_token_authentication_function
     */
    public function test_get_custom_token_function($service)
    {
        $this->assertNotNull($service->getCustomTokenFunction());
    }

    public function test_authenticated_function()
    {
        $func = function () {};

        $this->assertTrue(AuthService::setAuthenticatedFunction($func));
    }

    public function test_get_authenticated_user()
    {
        $token = "Test Token";

        $this->config->shouldReceive('get')
            ->once()
            ->with('clearlink-auth.users.baseUri')
            ->andReturn('http://testuri');

        $response = Mockery::mock();

        $this->client->shouldReceive('send')
            ->once()
            ->andReturnUsing(function ($request) use ($response) {
                $uri = $request->getUri();
                $this->assertEquals('testuri', $uri->getHost());
                $this->assertEquals('/api/v1/me', $uri->getPath());

                $this->assertEquals('Bearer Test Token', $request->getHeader("Authorization")[0]);
                $this->assertEquals('application/json', $request->getHeader('Accepts')[0]);

                return $response;
            });

        $response->shouldReceive('getBody->__toString')
            ->andReturn($json = "Test Json");

        $this->factory->shouldReceive('create')
            ->once()
            ->with($json)
            ->andReturn($user = Mockery::mock('Clearlink\Users\User'));

        $this->assertEquals($user, $this->service->getAuthenticatedUser($token));
    }

    public function test_get_authenticated_user_handles_error()
    {
        $this->assertTrue(false);
    }

    public function test_return_authorization_code_redirect()
    {
        $request = Mockery::mock('Illuminate\Http\Request');
        $request->shouldReceive('fullUrl')
            ->andReturn("URL");
        $request->shouldReceive('session->token')
            ->andReturn("token");

        $this->config->shouldReceive('get')
            ->once()
            ->with('clearlink-auth.sso.callback')
            ->andReturn('callback');
        $this->config->shouldReceive('get')
            ->once()
            ->with('clearlink-auth.sso.authEndpoint')
            ->andReturn('http://test.app');
        $this->config->shouldReceive('get')
            ->once()
            ->with('clearlink-auth.sso.clientId')
            ->andReturn('id');

        $response = $this->service->getAuthorizationCodeRedirect($request);
        $this->assertInstanceOf('Illuminate\Http\RedirectResponse', $response);
    }

    public function test_get_token_from_authorization_code()
    {
        $request = Mockery::mock('Illuminate\Http\Request');
        $request->shouldReceive('session')
            ->andReturn($session = Mockery::mock());

        $session->shouldReceive('token')
            ->andReturn("csrf");
        $session->shouldReceive('regenerate');

        $request->shouldReceive('only')
            ->once()
            ->with(['code', 'state'])
            ->andReturn([
                'code' => "Test Code",
                'state' => json_encode([
                    'location' => $originalUrl = "originalUrl",
                    'csrf' => "csrf"
                ])
            ]);

        $this->config->shouldReceive('get')
            ->once()
            ->with('clearlink-auth.sso.tokenEndpoint')
            ->andReturn($url = "tokenUrl");
        $this->config->shouldReceive('get')
            ->once()
            ->with('clearlink-auth.sso.callback')
            ->andReturn('callback');
        $this->config->shouldReceive('get')
            ->once()
            ->with('clearlink-auth.sso.clientId')
            ->andReturn('id');
        $this->config->shouldReceive('get')
            ->once()
            ->with('clearlink-auth.sso.clientSecret')
            ->andReturn('secret');
 
        self::$retval = 'http://test.com/';

        $response = Mockery::mock();
        $this->client->shouldReceive('post')
            ->once()
            ->andReturnUsing(function ($path, $options) use ($response, $url) {
                $this->assertEquals($url, $path);

                $this->assertEquals("Test Code", $options['form_params']['code']);
                $this->assertEquals("http://test.com/callback", $options['form_params']['redirect_uri']);

                return $response;
            });

        $response->shouldReceive('getBody->__toString')
            ->andReturn(json_encode([
                'access_token' => "Token",
                'refresh_token' => "Refresh Token"
            ]));

        $session->shouldReceive('put')
            ->with('clearlink_jwt', "Token");
        $session->shouldReceive('put')
            ->with('clearlink_refresh', "Refresh Token");

        $response = $this->service->getTokenFromAuthorizationCode($request);
    }
}
