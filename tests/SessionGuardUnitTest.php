<?php

use PHPUnit\Framework\TestCase;
use Clearlink\Auth\Guards\SessionGuard;

class SessionGuardUnitTest extends TestCase
{
    public function setup()
    {
        $this->request = Mockery::mock('Illuminate\Http\Request');
        $this->session = Mockery::mock();
        $this->request->shouldReceive('session')->andReturn($this->session);

        $this->provider = Mockery::mock('Illuminate\Contracts\Auth\UserProvider');
        $this->validator = Mockery::mock('Clearlink\Auth\JWT\TokenValidator');
        $this->service = Mockery::mock('Clearlink\Auth\AuthService');

        $this->guard = new SessionGuard($this->provider, $this->validator, $this->service);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function test_no_jwt()
    {
        $this->session->shouldReceive('get')
            ->with('clearlink_jwt')
            ->andReturn(null);

        $this->assertNull($this->guard->user($this->request));
    }
    
    public function test_invalid_jwt()
    {
        $this->session->shouldReceive('get')
            ->with('clearlink_jwt')
            ->andReturn($jwt = "Token");

        $this->validator->shouldReceive('validate')
            ->once()
            ->with($jwt)
            ->andReturn(\Clearlink\Auth\JWT\TokenValidator::INVALID);

        $this->assertNull($this->guard->user($this->request));
    }

    public function test_no_refresh()
    {
        $this->session->shouldReceive('get')
            ->with('clearlink_jwt')
            ->andReturn($jwt = "Token");

        $this->validator->shouldReceive('validate')
            ->once()
            ->with($jwt)
            ->andReturn(\Clearlink\Auth\JWT\TokenValidator::EXPIRED);

        $this->session->shouldReceive('get')
            ->once()
            ->with('clearlink_refresh')
            ->andReturn(null);

        $this->session->shouldReceive('forget')->with('clearlink_jwt');
        $this->session->shouldReceive('forget')->with('clearlink_user');
        $this->session->shouldReceive('forget')->with('clearlink_refresh');

        $this->assertNull($this->guard->user($this->request));
    }

    public function test_valid_jwt()
    {
        $this->session->shouldReceive('get')
            ->with('clearlink_jwt')
            ->andReturn($jwt = "Token");

        $this->validator->shouldReceive('validate')
            ->once()
            ->with($jwt)
            ->andReturn(\Clearlink\Auth\JWT\TokenValidator::VALID);

        $this->session->shouldReceive('get')
            ->once()
            ->with('clearlink_user')
            ->andReturn($user = Mockery::mock());

        $this->assertEquals($user, $this->guard->user($this->request));
    }

    public function test_user_from_provider()
    {
        $this->session->shouldReceive('get')
            ->with('clearlink_jwt')
            ->andReturn($jwt = "Token");

        $this->validator->shouldReceive('validate')
            ->once()
            ->with($jwt)
            ->andReturn(\Clearlink\Auth\JWT\TokenValidator::VALID);

        $this->session->shouldReceive('get')
            ->once()
            ->with('clearlink_user')
            ->andReturn(null);

        $this->provider->shouldReceive('retrieveByCredentials')
            ->once()
            ->with(['jwt' => $jwt])
            ->andReturn($user = Mockery::mock());

        $this->session->shouldReceive('put')
            ->once()
            ->with('clearlink_user', $user);

        $this->assertEquals($user, $this->guard->user($this->request));
    }

    public function est_valid_refresh_token()
    {
        //First invocation
        $this->session->shouldReceive('get')
            ->with('clearlink_jwt')
            ->andReturn($jwt = "Token");

        $this->validator
            ->shouldReceive('validate')
            ->with($jwt)
            ->andReturn(
                \Clearlink\Auth\JWT\TokenValidator::EXPIRED,
                \Clearlink\Auth\JWT\TokenValidator::VALID
            );

        //Refresh token
        $this->session->shouldReceive('get')
            ->once()
            ->with('clearlink_refresh')
            ->andReturn($token = 'refresh');

        $this->session->shouldReceive('forget')->with('clearlink_jwt');
        $this->session->shouldReceive('forget')->with('clearlink_user');
        $this->session->shouldReceive('forget')->with('clearlink_refresh');

        $this->service->shouldReceive('getTokenFromRefreshToken')
            ->with($token)
            ->andReturn($r = Mockery::mock());

        $r->access_token = 'new';
        $r->refresh_token = 'refresh';

        $this->session->shouldReceive('put')
            ->once()
            ->with('clearlink_jwt', $r->access_token);
        $this->session->shouldReceive('put')
            ->once()
            ->with('clearlink_refresh', $r->refresh_token);

        //Second invocation
        $this->session->shouldReceive('get')
            ->once()
            ->with('clearlink_user')
            ->andReturn(null);

        $this->provider->shouldReceive('retrieveByCredentials')
            ->once()
            ->with(['jwt' => $jwt])
            ->andReturn($user = Mockery::mock());

        $this->session->shouldReceive('put')
            ->once()
            ->with('clearlink_user', $user);

        $this->assertEquals($user, $this->guard->user($this->request));
    }
}
