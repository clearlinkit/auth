<?php

use PHPUnit\Framework\TestCase;
use Clearlink\Auth\Traits\ClearlinkUser;

class ClearlinkUserUnitTest extends TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    public function test_set_clearlink_user()
    {
        $stub = new StubClearlinkUser;
        $this->assertTrue($stub->setClearlinkUser(Mockery::mock('Clearlink\Users\BaseUser')));

        return $stub;
    }

    /**
     * @depends test_set_clearlink_user
     */
    public function test_get_clearlink_user($stub)
    {
        $this->assertNotNull($stub->clearlinkUser());
    }

    /**
     * @depends test_set_clearlink_user
     */
    public function test_wont_reset_user($stub)
    {
        $this->assertFalse($stub->setClearlinkUser(Mockery::mock('Clearlink\Users\BaseUser')));
    }

    public function test_set_jwt()
    {
        $stub = new StubClearlinkUser;

        $this->assertTrue($stub->setJwt("jwt"));

        return $stub;
    }

    /**
     * @depends test_set_jwt
     */
    public function test_wont_reset_jwt($stub)
    {
        $this->assertFalse($stub->setJwt('twj'));
    }

    /**
     * @depends test_set_jwt
     */
    public function test_get_jwt($stub)
    {
        $this->assertNotNull($stub->jwt());
    }

    public function test_has_role()
    {
        $u = new StubRoleClearlinkUser;
        $this->assertTrue($u->hasRole('role2'));
    }

    public function test_does_not_have_role()
    {
        $u = new StubRoleClearlinkUser;
        $this->assertFalse($u->hasRole('role5'));
    }

    public function test_clearlink_can()
    {
        $u = new StubPermissionClearlinkUser;
        $this->assertTrue($u->clearlinkCan('permission3'));
    }

    public function test_clearlink_cannot()
    {
        $u = new StubPermissionClearlinkUser;
        $this->assertTrue($u->clearlinkCannot('permission6'));
    }
}

class StubClearlinkUser
{
    use ClearlinkUser;
}

class StubRoleClearlinkUser
{
    use ClearlinkUser;

    private $baseUser;

    public function __construct()
    {
        $this->baseUser = Mockery::mock();

        $role = Mockery::mock();
        $role->shouldReceive('getName')
            ->andReturn('role1', 'role2', 'role3');

        $this->baseUser->shouldReceive('getRoles')
            ->andReturn([
                $role,
                $role,
                $role
            ]);
    }
}

class StubPermissionClearlinkUser
{
    use ClearlinkUser;

    private $baseUser;

    public function __construct()
    {
        $this->baseUser = Mockery::mock();

        $role = Mockery::mock();

        $this->baseUser->shouldReceive('getRoles')
            ->andReturn([
                $role
            ]);

        $perm = Mockery::mock();
        $perm->shouldReceive('getPermission')
            ->andReturn('permission1', 'permission2', 'permission3');

        $role->shouldReceive('getPermissions')
            ->andReturn([
                $perm,
                $perm,
                $perm
            ]);
    }
}
