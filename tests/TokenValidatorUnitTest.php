<?php

use PHPUnit\Framework\TestCase;
use Clearlink\Auth\JWT\TokenValidator;

class TokenValidatorUnitTest extends TestCase
{
    public function setup()
    {
        $this->parser = Mockery::mock('Lcobucci\JWT\Parser');
        $this->signer = Mockery::mock('Lcobucci\JWT\Signer\Rsa');
        $this->data = Mockery::mock('Lcobucci\JWT\ValidationData');
        $this->service = Mockery::mock('Clearlink\Auth\AuthService');

        $this->rawToken = "raw";

        $this->validator = new TokenValidator($this->parser, $this->signer, $this->data, $this->service);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function test_valid_jwt()
    {
        $this->parser->shouldReceive('parse')
            ->once()
            ->with($this->rawToken)
            ->andReturn($token = Mockery::mock());

        $this->service->shouldReceive('getPublicKey')
            ->andReturn($key = 'key');

        $token->shouldReceive('verify')
            ->with($this->signer, $key)
            ->andReturn(true);

        $token->shouldReceive('isExpired')
            ->andReturn(false);

        $this->assertEquals(TokenValidator::VALID, $this->validator->validate($this->rawToken));
    }

    public function test_expired_jwt()
    {
        $this->parser->shouldReceive('parse')
            ->once()
            ->with($this->rawToken)
            ->andReturn($token = Mockery::mock());

        $this->service->shouldReceive('getPublicKey')
            ->andReturn($key = 'key');

        $token->shouldReceive('verify')
            ->with($this->signer, $key)
            ->andReturn(true);

        $token->shouldReceive('isExpired')
            ->andReturn(true);

        $this->assertEquals(TokenValidator::EXPIRED, $this->validator->validate($this->rawToken));
    }

    public function test_invalid_signiture()
    {
        $this->parser->shouldReceive('parse')
            ->once()
            ->with($this->rawToken)
            ->andReturn($token = Mockery::mock());

        $this->service->shouldReceive('getPublicKey')
            ->andReturn($key = 'key');

        $token->shouldReceive('verify')
            ->with($this->signer, $key)
            ->andReturn(false);

        $this->assertEquals(TokenValidator::SIGNATURE_INVALID, $this->validator->validate($this->rawToken));
    }

    public function test_valid_old()
    {
        $this->service->shouldReceive('introspect')
            ->with($this->rawToken)
            ->andReturn($token = Mockery::mock());

        $token->active = true;

        $this->assertEquals(TokenValidator::VALID, $this->validator->validate($this->rawToken));
    }

    public function test_invalid_old()
    {
        $this->service->shouldReceive('introspect')
            ->with($this->rawToken)
            ->andReturn($token = Mockery::mock());

        $token->active = false;

        $this->assertEquals(TokenValidator::INVALID, $this->validator->validate($this->rawToken));
    }

    public function test_is_old_token_type()
    {
        $this->assertNull($this->validator->isOldStyleToken());
    }
}
