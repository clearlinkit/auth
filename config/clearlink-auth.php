<?php

return [
    'sso' => [
        'authEndpoint' => env('CL_SSO_URL') . '/v1/auth',
        'tokenEndpoint' => env('CL_SSO_URL') . '/v1/token/jwt/',
        'introspectEndpoint' => env('CL_SSO_URL') . '/v1/introspect/',
        'callback' => '/auth-callback',
        'clientId' => env('CL_SSO_CLIENT_ID'),
        'clientSecret' => env('CL_SSO_CLIENT_SECRET')
    ],

    'users' => [
        'baseUri' => env('CL_USERS_URL')
    ],

    //The repository for generating custom user objects.
    //The repository must return an instance of Illuminate\Contracts\Auth\Authenticatable and Clearlink\Auth\Traits\ClearlinkAuthUser.
    'userRepository' => null
];