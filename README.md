# Clearlink\Auth

***Note that this package is not yet stable and the api is subject to change***

This package is used to facilitate authentication with Clearlink's SSO and authorization via Clearlink's Permission microservice.

This package will also retreive a token on behalf of the client using OAuth2 client credentials grant type.

## Installation

***These instructions assume that you have already added Clearlink's composer repository to your project's composer.json file.***

First, require the package:

~~~
composer require clearlink/auth
~~~

This will also require the following packages:

~~~
clearlink/dto
clearlink/permissions
clearlink/users
~~~

In the projects `.env` file, add the following lines, replacing `{{client_id}}`, `{{client_secret}}`, `{{sso_url}}`, and `{{users_url}}` with their respective values for your application:

```text
CL_SSO_CLIENT_ID={{client_id}}
CL_SSO_CLIENT_SECRET={{client_secret}}
CL_SSO_URL={{sso_url}}
CL_USERS_URL={{users_url}}
```

***In the future, the following step will no longer be required once the key is placed in a publically available location.***

You will then need to create a file in `storage/app/` called `cl_auth_public.pem`. 
The contents for this file is the public key set in the SSO application configuration.

From here, the installation differs slightly from Laravel to Lumen. The Laravel instructions are given first, followed by the Lumen instructions.

### Laravel Installation

Add the following lines to the projects `config\app.php` file in the `'providers'` array:

```php
/*
 * Package Service Providers...
 */
Clearlink\Auth\AuthServiceProvider::class,
Clearlink\DTO\DTOServiceProvider::class
```

Both `clearlink/auth` and `clearlink/dto` have configuration files, but the `clearlink/auth` config only needs to be published if you are connecting to the UAT environment or if you are wanting to use an instance of a locally defined user. To publish to `clearlink/dto` config file, run the following:

```bash
php artisan vendor:publish --tag=clearlink-dto
```

The file will be published to `config/clearlink-dto.php` and will contain an array with a key of `modules`. You will need to uncomment the first two entries in `modules` so that your file looks like the following:

```php
return [
    'modules' => [
        \Clearlink\Users\Factory::class,
        \Clearlink\Permissions\Factory::class,
        // Possible other entries...
    ]
];
```

This will allow the `clearlink\auth` package to marshal JSON representation of objects into hard-typed objects.

***Configuration for using the UAT environment or a custom user can be found in a later section.***

Modify the `app\Exceptions\Handler->unauthenticated` to redirect to SSO in the case of not being authenticated:
```php
protected function unauthenticated($request, AuthenticationException $exception)
{
    if ($request->expectsJson()) {
        return response()->json(['error' => 'Unauthenticated.'], 401);
    }

    //replace this line
    //return redirect()->guest('login');
    //with this line
    return app(AuthService::class)->getAuthorizationCodeRedirect($request);
}
```

Update `config/auth.php` to use the provided authorization guards and user provider. Simply prepend `'clearlink-'` to both of the drivers for the `api` and `web` authentication guards, and change the driver for the `users` provider to `'clearlink'`:
```php
'guards' => [
    'web' => [
        'driver' => 'clearlink-session',
        'provider' => 'users',
    ],

    'api' => [
        'driver' => 'clearlink-token',
        'provider' => 'users',
    ],
],

'providers' => [
    'users' => [
        'driver' => 'clearlink',
    ],
],
```

Finally, you need to add the `auth` middleware to the routes that you want to protect. Please see the Laravel documentation for adding middleware to routes.

Now, whenver you call `$request->user()` or `Auth::user()`, you will get an instance of `Clearlink\Auth\User`.

### Lumen Installation

Open `bootstrap/app.php` and add the following lines in the service providers section (around line 72):

```php
$app->register(Clearlink\DTO\DTOServiceProvider::class);
$app->register(Clearlink\Auth\AuthServiceProvider::class);
```

***Note: In Lumen, it matters what order the service providers are registered. As of Lumen 5.3, a providers boot method is called immediatly following the register method whereas it should call all provider register methods followed by all the provider boot methods.***

While still in `bootstrap/app.php`, uncomment the lines adding the `auth` middleware to the application (around line 68):

```php
$app->routeMiddleware([
    'auth' => App\Http\Middleware\Authenticate::class,
]);
```

Lumen does not contain the `artisan vendor:publish` command, so the `clearlink-dto.php` config file will need to be copied from the vendor folder to the config folder:

```sh
cp vendor/clearlink/dto/config/clearlink-dto.php config/
```

If you plan on using a custom user, you will also need to publish the `clearlink-auth.php` config file:

```sh
cp vendor/clearlink/auth/config/clearlink-auth.php config/
```

The `clearlink-dto.php` config file will contain an array with a key of `modules`. You will need to uncomment the first two entries in `modules` so that your file looks like the following:

```php
return [
    'modules' => [
        \Clearlink\Users\Factory::class,
        \Clearlink\Permissions\Factory::class,
        // Possible other entries...
    ]
];
```

This will allow the `clearlink\auth` package to marshal JSON representation of objects into hard-typed objects.

All that is left to do is to add the `auth` middleware to the routes that you want to protect. Please see the Laravel documentation for adding middleware to routes.

### Additional Configuration

#### UAT Environment

***Note: This is only applicable for Laravel installations.***

If you are connecting to the UAT environment, you will also need to publish the `clearlink/auth` configuration file with the following:

```bash
php artisan vendor:publish --tag=clearlink-auth
```

In the config, find the key `authEndpoint` and add a trailing slash to the value so that it is:

```php
'authEndpoint' => env('CL_SSO_URL') . '/v1/auth/'
```

***NOTE: Typically, the config files are source controlled. The configuration in the clearlink-auth file needs to be different between UAT and non-UAT environments. The default configuration in the file is for non-UAT environments so it is acceptable to .gitignore the clearlink-auth.php config file.***

#### Custom User Objects

The `clearlink/auth` package by default returns a user of type `Clearlink\Auth\User` but you can override the package to return and object of your choice. If you choose to override the default object, the object that you wish to use __MUST__ implement `Illuminate\Contracts\Auth\Authenticatable` and have the trait `Clearlink\Auth\Traits\ClearlinkUser`. The default user in Laravel\Lumen implement the interface `Illuminate\Contracts\Auth\Authenticatable`, so all that will be needed if you wish to use that class are to add the trait to the `User` class:

```php
<?php

namespace App;

...
use Clearlink\Auth\Traits\ClearlinkAuthUser;
use Illuminate\Foundation\Auth\User as Authenticatable;
...

class User extends Authenticatable
{
    use Notifiable, ClearlinkAuthUser;

    ...
}

```

To override the returned user, you must specify `userRepository` in the `clearlink/auth` configuration file. That file can be published by:

```sh
#For Laravel:
php artisan vendor:publish --tag=clearlink-auth

#For Lumen:
cp vendor/clearlink/auth/config/clearlink-auth.php config/
```

The object that is specified must have a function with the signature `find($id)` where `$id` is the DNA User Id.
```php
'userRepository' => new \App\User
```

If you do override the default user, and the repository you specify returns `NULL`, the object does not implement `Illuminate\Contracts\Auth\Authenticatable`, or the object does not have the trait `Clearlink\Auth\Traits\ClearlinkUser`, and exception will be thrown.

#### Post Authorization

It can be helpful to execute code after a user is authenticated, and this package provides a means to can a function after a user is authenticated to your project. The specified function will run after the user object is retrieved from the Users Microservice but before a User is returned from the `UserProvider`. This can be helpful when overriding the user to create a user in your project's database. 

The function specified __MUST__ take a single arguement of type `Clearlink\Users\BaseUser`. It needs to be `BaseUser` because both applications and regular (human) users can authenticate to your system and they have different classes that extend from `BaseUser`. The most common class will be `User` (this will be humans) while application users will be of type `ApplicationUser`.

To specify the function, place the following in the `boot` method of your `AppServiceProvider`, replacing the function with the function of your choice:

```php
$this->app[\Clearlink\Auth\AuthService::class]->setAuthenticatedFunction(function (\Clearlink\Users\BaseUser $user){});
```

***Note: If this is a Lumen installation, the `AppServiceProvider` must be registered after the `Clearlink/Auth/AuthServiceProvider`.***

#### Logging Out

***Note: This is only applicable for Laravel installations.***

The `AuthService` class provides a function that will flush the session and generate a `RedirectResponse` that you can use to logout from SSO. Something similar to this in your application's route file will successfully log the user out from your application and SSO:

```php
Route:get('/logout', function(Request $request) {
    return app(\Clearlink\Auth\AuthService::class)->getLogoutRedirect($request);
});
```

***Note: There is currently a bug in SSO that will not allow applications using the `clearlink/auth` package to log back in from the logout page. Instead, your user will need to logout, then navigate back to the root url of your application which will then redirect to the SSO login page.***