# Release Notes for 0.1.x

## v0.1.7 (2017-11-14)

### Add

- Static method to AuthService (AuthService::getClientJWT) to create, cache, renew and return client JWT

### Fixed

- Use JWT->jti as cache key when caching user objects in TokenGuard
- Refactor various Guzzle requests to use client->[method] instead of construtor instantiation to fix errors being thrown

### Change

- Better error handling in auth-callback to redirect user to valid authentication request upon error 
 
## v0.1.6 (2017-07-03)

### Change

- Cache users for lumen requests

## v0.1.5.2 (2017-06-16)

### Fixed

- Fixed issue in retrieving token from refresh token
- Fixed bleeding exceptions from retrieving token from refresh token

## v0.1.5.1 (2017-06-13)

### Fixed

- Fixed `User@getKey` to call `getId()` instead of accessing protected property `id`

## v0.1.5 (2017-05-30)

### Added

- Added `jwt()` method to `ClearlinkUser` trait.

### Changed

- Refactored `AuthService@getTokenFromRefreshToken` to not interact with session.
- Refactored `SessionGuard` to update session when retrieving token from refresh token.

### Fixed

- Fixed `ClearlinkUser@clearlinkCannot` to call `clearlinkCan` instead of `can`.