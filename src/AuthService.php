<?php

namespace Clearlink\Auth;

use GuzzleHttp\Client;
use Clearlink\DTO\Factory;
use Illuminate\Http\Request;
use Clearlink\Users\BaseUser;
use Illuminate\Http\RedirectResponse;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Cache\Repository as CacheRepository;

class AuthService
{
    private static $user_unauthorized_function;
    private static $authenticated_function;
    private static $client_token_cache_key = 'clearlink.auth.client-token';
    
    private $config;
    private $cache;
    private $factory;
    private $client;
    private $customTokenAuthenticationFunction;

    public function __construct(
        ConfigRepository $config,
        CacheRepository $cache,
        Factory $factory,
        Client $client
    ) {
        $this->config   = $config;
        $this->cache    = $cache;
        $this->factory  = $factory;
        $this->client   = $client;
    }

    public function setCustomTokenAuthenticationFunction(callable $func)
    {
        $this->customTokenAuthenticationFunction = $func;

        return true;
    }

    public function getCustomTokenFunction()
    {
        return $this->customTokenAuthenticationFunction;
    }

    public static function setAuthenticatedFunction($func)
    {
        self::$authenticated_function = $func;

        return true;
    }

    public static function setUnauthorizedFunction($func)
    {
        self::$user_unauthorized_function = $func;
    }

    public static function setClientTokenCacheKey($key)
    {
        self::$client_token_cache_key = $key;
    }

    /**
     * Get a client_credentials JWT for this client
     *
     * @param CacheRepository $cache
     * @return string Client JWT
     * @throws \Exception
     */
    public static function getClientJWT(){

        try {

            //get application cache repository
            $cache = app('cache');

            //check token endpoint to be sure it is the JWT endpoint
            $tokenEndpoint = config('clearlink-auth.sso.tokenEndpoint');
            if (!strpos($tokenEndpoint, 'jwt')) {
                throw new \Exception("Configured endpoint for retrieving JWTs is invalid.");
            } else {

                //try to the fetch client token from cache
                $clientToken = $cache->get(self::$client_token_cache_key);

                //is token missing or expired?
                if (!$clientToken || $clientToken['expiresOn'] < time()) {

                    //client token is not found in cache (or is expired), fetch a new one from tokenEndpoint
                    $client = new Client();

                    $result = $client->request('POST', $tokenEndpoint, [
                        'headers' => [
                            'Authorization' => 'Basic ' . base64_encode(config('clearlink-auth.sso.clientId') . ":" . config('clearlink-auth.sso.clientSecret')),
                            'Accept' => 'application/json',
                            'Content-Type' => 'application/x-www-form-urlencoded'
                        ], 'form_params' => [
                            'grant_type' => 'client_credentials'
                        ]
                    ]);

                    //make sure a valid token response was received
                    if ($result->getStatusCode() !== 200) {
                        throw new \Exception("JWT could not be obtained: " . $result->getBody(), 1);
                    }

                    $jwt = json_decode($result->getBody())->access_token;

                    $clientToken = array('access_token' => $jwt, 'expiresOn' => json_decode(base64_decode(explode('.',$jwt)[1]))->exp);

                    $cache->forever(self::$client_token_cache_key, $clientToken);
                }

                //only return the access_token from the token
                return $clientToken['access_token'];
            }

        }catch(\Exception $e){
            return "";
        }
    }

    //Todo: This needs better error handling
    public function getAuthenticatedUser($token)
    {

        try {
            $response = $this->client->get(
                $this->config->get('clearlink-auth.users.baseUri')."/api/v1/me",
                [
                    'headers' => [
                        'Authorization' => "Bearer $token",
                        'Accepts' => 'application/json'
                    ]
                ]
            );

            $json = $response->getBody()->__toString();

            if ($user = $this->factory->create($json)) {
                $this->authenticated($user);
            }

            return $user;
        } catch (RequestException $e) {
            throw new \Exception("There was an error retrieving the authenticated user from the User Microservice", 1, $e);
        }
    }

    public function authenticated(BaseUser $user)
    {
        if (isset(self::$authenticated_function)) {
            call_user_func(static::$authenticated_function, $user);
        }
    }

    public function getAuthorizationCodeRedirect(Request $request)
    {
        $params = [
            'redirect_uri' => env('APP_URL') . $this->config->get('clearlink-auth.sso.callback'),
            'scope' => 'read',
            'state' => json_encode([
                'location' => $request->fullUrl(),
                'csrf' => $request->session()->token()
            ])
        ];

        return new RedirectResponse($this->config->get('clearlink-auth.sso.authEndpoint')
            ."?".http_build_query(array_merge([
                'client_id' => $this->config->get('clearlink-auth.sso.clientId'),
                'response_type' => 'code'
            ], $params)));
    }

    public function getTokenFromAuthorizationCode(Request $request)
    {
        $queryParams = $request->only(['code', 'state']);

        $code = $queryParams['code'];
        $state = json_decode($queryParams['state']);

        if ($state == null) {
            throw new \Exception("The SSO application did not return the state object that was sent with the Authorization Code Request", 1);
        }

        if ($state->csrf != $request->session()->token()) {
            return;
        }

        $response = $this->client->post($this->config->get('clearlink-auth.sso.tokenEndpoint'), [
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode($this->config->get('clearlink-auth.sso.clientId').":".$this->config->get('clearlink-auth.sso.clientSecret')),
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => env('APP_URL') . $this->config->get('clearlink-auth.sso.callback'),
                'scope' => 'read'
            ]
        ]);

        if ($token = json_decode($response->getBody()->__toString())) {
            $session = $request->session();

            $session->regenerate();

            $session->put('clearlink_jwt', $token->access_token);
            if ($token->refresh_token) {
                $session->put('clearlink_refresh', $token->refresh_token);
            }

            return new RedirectResponse($state->location);
        }
    }

    public function getTokenFromRefreshToken($refreshToken)
    {

        try {
            $response = $this->client->post($this->config->get('clearlink-auth.sso.tokenEndpoint'), [
                'headers' => [
                    'Authorization' => 'Basic ' . base64_encode($this->config->get('clearlink-auth.sso.clientId').":".$this->config->get('clearlink-auth.sso.clientSecret')),
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $refreshToken,
                    'scope' => 'read'
                ]
            ]);

            return json_decode($response->getBody()->__toString());
        } catch (\Exception $e) {
            throw new \Exception("There was an error retrieving a token from refresh token.", 1, $e);
        }
    }

    public function getLogoutRedirect(Request $request)
    {
        $session = $request->session();

        $session->flush();
        $session->regenerate();
        $session->regenerateToken();

        $params = [
            'redirect_uri' => env('APP_URL') . $this->config->get('clearlink-auth.sso.callback'),
            'scope' => 'read',
            'state' => json_encode([
                'location' => $request->root(),
                'csrf' => $session->token()
            ])
        ];

        return new RedirectResponse($this->config->get('clearlink-auth.sso.authEndpoint')."?".http_build_query(array_merge([
            'killTokenSession' => 1,
            'killTokenSessionFeedback' => 'You have been successfully logged out.',
            'client_id' => $this->config->get('clearlink-auth.sso.clientId'),
            'response_type' => 'code'
        ], $params)));
    }

    public function generateUnauthenticatedResponse()
    {
        if (isset(self::$user_unauthorized_function)) {
            return call_user_func(self::$user_unauthorized_function);
        }

        return view('cl-auth::401');
    }

    //TODO: This needs error handling
    public function getClientCredentialsToken()
    {
        try {

            return self::getClientJWT();

        }catch(\Exception $e){
            return "";
        }
    }

    public function introspect($token)
    {
        if ($introspect = $this->cache->get("clearlink:auth:$token")) {
            $introspect = json_decode($introspect);

            if ($introspect->exp > time()) {
                return $introspect;
            }
            
            $this->cache->forget("clearlink:auth:$token");
            $introspect = null;
        }

        try {
            $response = $this->client->post($this->config->get('clearlink-auth.sso.introspectEndpoint'), [
                'headers' => [
                    'Authorization' => "Bearer $token",
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    "token" => base64_decode($token)
                ]
            ]);

            $json = $response->getBody()->__toString();
            $this->cache->put("clearlink:auth:$token", $json, 60);
            $introspect = json_decode($json);
        } catch (RequestException $e) {
            $introspect = new \stdClass();
            $introspect->active = false;
        }

        return $introspect;
    }

    public function getPublicKey()
    {
        if ($key = $this->cache->get("public_key")) {
            return $key;
        }

        if (! $key = file_get_contents(storage_path('app/cl_auth_public.pem'))) {
            throw new Exception("The cl_auth_public.pem file is empty or does not exist", 1);
        }
        
        $this->cache->forever("public_key", $key);
        
        return $key;
    }

    public function getCustomUserRepository()
    {
        return $this->config->get('clearlink-auth.userRepository');
    }
}
