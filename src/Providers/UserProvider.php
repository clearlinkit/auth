<?php

namespace Clearlink\Auth\Providers;

use Clearlink\Auth\User;
use Clearlink\Users\BaseUser;
use Clearlink\Auth\AuthService;
use Illuminate\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider as Contract;

class UserProvider implements Contract
{
    private $service;
    private $cache;

    public function __construct(
        AuthService $service,
        CacheRepository $cache
    ) {
        $this->service = $service;
        $this->cache = $cache;
    }

    public function retrieveByCredentials(array $credentials)
    {
        $dtoUser = null;
        if (isset($credentials['cacheKey'])) {
            $dtoUser = $this->cache->get($credentials['cacheKey']);
        }

        if (! $dtoUser) {
            $dtoUser = $this->service->getAuthenticatedUser($credentials['jwt']);

            if (isset($credentials['cacheKey'])) {
                $this->cache->put($credentials['cacheKey'], $dtoUser, 20);
            }
        }

        $user = $this->getCustomUser($dtoUser) ?: new User($dtoUser);

        $user->setJwt($credentials['jwt']);

        return $user;
    }

    protected function getCustomUser(BaseUser $user)
    {
        if (! $repo = $this->service->getCustomUserRepository()) {
            return;
        }
            
        if (! $customUser = $repo->find($user->getId())) {
            throw new \Exception("A custom user model has been defined but a model could not be retrieved for user ".$user->getId(), 1);
        }

        if (! $customUser instanceof \Illuminate\Contracts\Auth\Authenticatable) {
            throw new \Exception("The custom user object does not implement Illuminate\Contracts\Auth\Authenticatable", 1);
        }

        if (! array_key_exists(\Clearlink\Auth\Traits\ClearlinkUser::class, class_uses($customUser))) {
            throw new \Exception("The custom user object does not implement Clearlink\Auth\Traits\ClearlinkAuthUser", 1);
        }

        $customUser->setClearlinkUser($user);
        
        return $customUser;
    }

    public function retrieveById($identifier)
    {
        throw new \Exception('retrieveById is not implemented');
    }

    public function retrieveByToken($identifier, $token)
    {
        throw new \Exception('retrieveByToken is not implemented');
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        throw new \Exception('updateRememberToken is not implemented');
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        throw new \Exception('validateCredentials is not implemented');
    }
}
