<?php

use Illuminate\Http\Request;
use Clearlink\Auth\AuthService;
use Illuminate\Http\RedirectResponse;

Route::get('/auth-callback', function (Request $request) {

    $service = app(AuthService::class);
    try {

        if (!$response = $service->getTokenFromAuthorizationCode($request, $request->code)) {
            return $service->generateUnauthenticatedResponse();
        }

    }catch(Exception $e){

        $params = [
            'redirect_uri' => env('APP_URL') . config('clearlink-auth.sso.callback'),
            'scope' => 'read',
            'state' => json_encode([
                'location' => env('APP_URL'),
                'csrf' => $request->session()->token()
            ])
        ];

        return new RedirectResponse(config('clearlink-auth.sso.authEndpoint')
            ."?".http_build_query(array_merge([
                'client_id' => config('clearlink-auth.sso.clientId'),
                'response_type' => 'code'
            ], $params)));

    }

    return $response;
})->middleware('web');
