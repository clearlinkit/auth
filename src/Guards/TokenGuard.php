<?php

namespace Clearlink\Auth\Guards;

use Illuminate\Http\Request;
use Clearlink\Auth\JWT\TokenValidator;
use Illuminate\Contracts\Auth\UserProvider;

class TokenGuard
{
    private $provider;
    private $validator;
    
    public function __construct(
        UserProvider $provider,
        TokenValidator $validator
    ) {
        $this->provider = $provider;
        $this->validator = $validator;
    }

    public function user(Request $request)
    {
        if (!$token = $request->bearerToken()) {
            return;
        }

        if ($this->validator->validate($token) !== TokenValidator::VALID) {
            return;
        }

        $cacheKey = $token;
        if(!$this->validator->isOldStyleToken()){
            $payload = json_decode(base64_decode(explode('.', $token)[1]));

            if($payload->jti){
                $cacheKey = $payload->jti;
            }else{
                return;
            }

        }
        
        return $this->provider->retrieveByCredentials([
            'jwt' => $token,
            'cacheKey' => $cacheKey
        ]);
    }
}
