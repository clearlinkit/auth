<?php

namespace Clearlink\Auth\Guards;

use Clearlink\Auth\AuthService;
use Clearlink\Auth\JWT\TokenValidator;
use Illuminate\Contracts\Auth\UserProvider;
use Symfony\Component\HttpFoundation\Request;

class SessionGuard
{
    private $provider;
    private $validator;
    private $service;
    
    public function __construct(
        UserProvider $provider,
        TokenValidator $validator,
        AuthService $service
    ) {
        $this->provider = $provider;
        $this->validator = $validator;
        $this->service = $service;
    }

    public function user(Request $request)
    {
        $token = $request->session()->get('clearlink_jwt');

        if (is_null($token)) {
            return;
        }

        $validationResult = $this->validator->validate($token);

        if ($validationResult === TokenValidator::EXPIRED) {
            return $this->userFromRefresh($request);
        } elseif ($validationResult !== TokenValidator::VALID) {
            return;
        }

        if (! $user = $request->session()->get('clearlink_user')) {
            $user = $this->provider->retrieveByCredentials(['jwt' => $token]);

            $request->session()->put('clearlink_user', $user);
        }

        return $user;
    }

    private function userFromRefresh($request)
    {
        $session = $request->session();

        $refresh = $session->get('clearlink_refresh');

        $session->forget('clearlink_jwt');
        $session->forget('clearlink_user');
        $session->forget('clearlink_refresh');

        if (! $refresh) {
            return;
        }

        if ($result = $this->service->getTokenFromRefreshToken($refresh)) {
            $session->put('clearlink_jwt', $result->access_token);
            if ($result->refresh_token) {
                $session->put('clearlink_refresh', $result->refresh_token);
            }
        }

        return $this->user($request);
    }
}
