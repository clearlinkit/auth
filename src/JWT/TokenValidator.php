<?php

namespace Clearlink\Auth\JWT;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Rsa;
use Lcobucci\JWT\ValidationData;

use Clearlink\Auth\AuthService;

class TokenValidator
{
    const VALID = 1;
    const EXPIRED = 2;
    const SIGNATURE_INVALID = 3;
    const INVALID = 4;

    private $parser;
    private $signer;
    private $validationData;
    private $service;

    private $token;
    private $oldStyleToken;

    public function __construct(Parser $parser, Rsa $signer, ValidationData $data, AuthService $service)
    {
        $this->parser           = $parser;
        $this->signer           = $signer;
        $this->validationData   = $data;
        $this->service          = $service;
    }

    public function validate($rawToken)
    {
        try {
            $this->token = $this->parser->parse($rawToken);
            $this->oldStyleToken = false;

            return $this->validateJWT();
        } catch (\Exception $e) {
            $this->token = $rawToken;
            $this->oldStyleToken = true;

            return $this->validateOld($rawToken);
        }
    }

    public function token()
    {
        return $this->token;
    }

    private function validateJWT()
    {
        if (! $this->token->verify($this->signer, $this->service->getPublicKey())) {
            return self::SIGNATURE_INVALID;
        }

        if ($this->token->isExpired()) {
            return self::EXPIRED;
        }

        return self::VALID;
    }

    public function validateOld($rawToken)
    {
        $this->token = $this->service->introspect($rawToken);
        
        if (! $this->token->active) {
            return self::INVALID;
        }

        return self::VALID;
    }

    public function isOldStyleToken()
    {
        return $this->oldStyleToken;
    }
}
