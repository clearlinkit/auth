<?php

namespace Clearlink\Auth;

use Clearlink\Auth\Guards\TokenGuard;
use Clearlink\Auth\Guards\SessionGuard;
use Clearlink\Auth\Providers\UserProvider;

use Illuminate\Auth\RequestGuard;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $isLumen = $this->isLumen();

        if ($this->app->runningInConsole() && !$isLumen) {
            $this->publishes([
                __DIR__.'/../config/clearlink-auth.php' => config_path('clearlink-auth.php'),
            ], 'clearlink-auth');
        }

        $this->setGuards($isLumen);
        
        if (! $isLumen) {
            $this->setProvider();
            $this->loadRoutesFrom(__DIR__.'/routes/auth-routes.php');
            $this->loadViewsFrom(__DIR__.'/views', 'cl-auth');
        }
    }

    public function register()
    {
        $isLumen = $this->isLumen();

        if ($isLumen) {
            $this->app->configure('clearlink-auth');
        }

        $this->mergeConfigFrom(
            __DIR__.'/../config/clearlink-auth.php',
            'clearlink-auth'
        );

        $this->app->singleton('Clearlink\Auth\AuthService', function ($app) {
            return new AuthService(
                $app['config'],
                $app['cache.store'],
                $app[\Clearlink\DTO\Factory::class],
                new \GuzzleHttp\Client
            );
        });

        $this->app->singleton('Clearlink\Auth\JWT\TokenValidator', function ($app) {
            return new JWT\TokenValidator(
                new \Lcobucci\JWT\Parser,
                new \Lcobucci\JWT\Signer\Rsa\Sha256,
                new \Lcobucci\JWT\ValidationData,
                $app['Clearlink\Auth\AuthService']
            );
        });
    }

    private function setProvider()
    {
        $this->app['auth']->provider('clearlink', function ($app, array $config) {
            return $this->createUserProvider();
        });
    }

    private function setGuards($isLumen)
    {
        $apiKey = $isLumen ? 'api' : 'clearlink-token';

        if ($isLumen) {
            $this->app['auth']->viaRequest('api', function ($request) {
                if ($func = $this->app[AuthService::class]->getCustomTokenFunction()) {
                    return call_user_func($func, $request);
                }

                return $this->makeTokenGuard()->user($request);
            });
        } else {
            $this->app['auth']->extend('clearlink-token', function ($app, $name, array $config) {
                return $this->makeRequestGuard([$this, 'makeTokenGuard'], $config);
            });

            if (! $isLumen) {
                $this->app['auth']->extend('clearlink-session', function ($app, $name, array $config) {
                    return $this->makeRequestGuard([$this, 'makeSessionGuard'], $config);
                });
            }
        }
    }

    private function makeRequestGuard(callable $func, $config)
    {
        $guard = new RequestGuard(function ($request) use ($func, $config) {
                return $func($config)->user($request);
        }, $this->app['request']);

        $this->app->refresh('request', $guard, 'setRequest');

        return $guard;
    }

    private function makeSessionGuard(array $config)
    {
        return new SessionGuard(
            $this->app['auth']->createUserProvider($config['provider']),
            $this->app['Clearlink\Auth\JWT\TokenValidator'],
            $this->app['Clearlink\Auth\AuthService']
        );
    }

    private function makeTokenGuard(array $config = null)
    {
        $provider = isset($config)
            ? $this->app['auth']->createUserProvider($config['provider'])
            : $this->createUserProvider();

        return new TokenGuard(
            $provider,
            $this->app['Clearlink\Auth\JWT\TokenValidator']
        );
    }

    public function createUserProvider()
    {
        return new UserProvider(
            $this->app[AuthService::class],
            $this->app['cache.store']
        );
    }

    protected function isLumen()
    {
        return str_contains($this->app->version(), 'Lumen');
    }
}
