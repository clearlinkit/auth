<?php

namespace Clearlink\Auth\Traits;

use Clearlink\Users\BaseUser;

trait ClearlinkUser
{
    private $baseUser;
    private $jwt;

    public function clearlinkUser()
    {
        return $this->baseUser;
    }

    public function setClearlinkUser(BaseUser $user)
    {
        if ($this->baseUser) {
            return false;
        }
        
        $this->baseUser = $user;

        return true;
    }

    public function jwt()
    {
        return $this->jwt;
    }

    public function setJwt($jwt)
    {
        if ($this->jwt) {
            return false;
        }
        
        $this->jwt = $jwt;

        return true;
    }

    public function clearlinkCan($ability, $arguments = [])
    {
        foreach ($this->getPermissions() as $permission) {
            if ($permission->getPermission() == $ability) {
                return true;
            }
        }
        

        return false;
    }

    public function clearlinkCannot($ability, $arguments = [])
    {
        return ! $this->clearlinkCan($ability, $arguments);
    }

    public function hasRole($roleName)
    {
        if ($roles = $this->baseUser->getRoles()) {
            foreach ($roles as $role) {
                if ($role->getName() == $roleName) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getPermissions()
    {
        $roles = $this->baseUser->getRoles();

        if (! $roles) {
            return;
        }

        foreach ($roles as $role) {
            if (! $permissions = $role->getPermissions()) {
                continue;
            }

            foreach ($permissions as $permission) {
                yield $permission;
            }
        }
    }
}
