<?php

namespace Clearlink\Auth;

use Clearlink\Users\BaseUser;
use Illuminate\Auth\Authenticatable;
use Clearlink\Auth\Traits\ClearlinkUser;
use Illuminate\Contracts\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User implements AuthenticatableContract, Authorizable
{
    use Authenticatable, ClearlinkUser {
        ClearlinkUser::clearlinkCan as can;
        ClearlinkUser::clearlinkCannot as cannot;
    }

    public function __construct(BaseUser $user)
    {
        $this->setClearlinkUser($user);
    }

    public function getKeyName()
    {
        return 'id';
    }

    public function getKey()
    {
        return $this->clearlinkUser()->getId();
    }

    public function __call($method, $arguments)
    {
        return $this->clearlinkUser()->{$method}(...$arguments);
    }

    public function serialize()
    {
        return serialize($this->clearlinkUser());
    }

    public function unserialize($serialized)
    {
        $this->setClearlinkUser(unserialize($serialized));
    }
}
